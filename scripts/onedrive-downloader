#!/usr/bin/env python3
# vim: set ts=8 sts=4 et sw=4 tw=99:
#
# Probes for new meets from GPC-AUS.
# Note that to use this script you will need Selenium installed
# and geckodriver on your path.

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import os
import sys
import time
import shutil


def download_file(url, resultsname):
    profile = webdriver.FirefoxProfile()

    profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                           "application/"
                           "vnd.openxmlformats-officedocument.spreadsheetml.sheet")

    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.dir", os.getcwd())

    foptions = Options()
    foptions.headless = True
    driver = webdriver.Firefox(profile, options=foptions)
    driver.get(url)

    time.sleep(2)

    iframe = driver.find_element_by_id('sdx_ow_iframe')
    driver.switch_to.frame(iframe)

    download_button = driver.find_element_by_id(
        "m_excelWebRenderer_ewaCtl_btnDownload-Medium20")
    download_button.click()

    time.sleep(1)

    alertbox = driver.find_elements_by_id("errorbuttonarea")
    if len(alertbox) != 0:
        yes_button = alertbox[0].find_element_by_class_name("ewa-dlg-button")
        yes_button.click()

    driver.quit()

    if os.path.exists("geckodriver.log"):
        os.remove("geckodriver.log")

    # Rename the file
    filename = sorted([os.path.join(os.getcwd(), basename)
                       for basename in os.listdir(os.getcwd())], key=os.path.getctime)[0]
    shutil.move(filename, os.path.join(os.getcwd(), resultsname))


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: %s url results-name" % sys.argv[1])
    download_file(sys.argv[1], sys.argv[2])
